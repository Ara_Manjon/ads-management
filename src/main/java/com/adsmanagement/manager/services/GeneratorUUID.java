package com.adsmanagement.manager.services;

import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class GeneratorUUID implements GeneratorID {
  public String generate(){

    return UUID.randomUUID().toString();
  }
}
