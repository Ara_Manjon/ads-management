package com.adsmanagement.manager.services;

import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class TimestampServer implements TimeServer {
    @Override
    public Timestamp getDate() {
        return new Timestamp(new Date().getTime());
    }
}
