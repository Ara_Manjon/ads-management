package com.adsmanagement.manager.managerservices.infrastructure;

public interface GeneratorID {
  String generate();
}
