package com.adsmanagement.manager.managerservices.infrastructure;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public interface TimeServer {
    Timestamp getDate();
}
