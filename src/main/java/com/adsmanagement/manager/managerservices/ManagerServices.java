package com.adsmanagement.manager.managerservices;

import com.adsmanagement.adsmanager.domain.Manager;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Identifier;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.Repository;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;

public class ManagerServices implements Manager {

  private final Repository repository;
  private final GeneratorID generatorID;
  private final TimeServer timeServer;

  public ManagerServices(Repository repository, GeneratorID generatorID, TimeServer timeServer) {
    this.repository = repository;
    this.generatorID = generatorID;
    this.timeServer = timeServer;
  }

  @Override
  public Identifier generateId() {
    return new Id(generatorID.generate());
  }

  @Override
  public Date generateDate() {
    return new Date(timeServer.getDate());
  }

  @Override
  public Repository generateRepository() {
    return this.repository;
  }
}
