package com.adsmanagement.adsmanager.infrastructure;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public interface DTO {

  class Ad {

    public String id;
    public String title;
    public String description;
    public Timestamp date;

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Ad adDTO = (Ad) o;
      return Objects.equals(id, adDTO.id) &&
          Objects.equals(title, adDTO.title) &&
          Objects.equals(description, adDTO.description) &&
          Objects.equals(date, adDTO.date);
    }

    @Override
    public int hashCode() {
      return Objects.hash(id, title, description, date);
    }

    @Override
    public String toString() {
      return "Ad{" +
          "id='" + id + '\'' +
          ", title='" + title + '\'' +
          ", description='" + description + '\'' +
          ", date=" + date +
          '}';
    }
  }

  class ListAds implements Iterable<Ad>{

    public final List<Ad> list = new ArrayList<>();

    public void add(Ad ad) {
      list.add(ad);
    }

    @Override
    public Iterator<Ad> iterator() {
      return this.list.iterator();
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      ListAds that = (ListAds) o;
      return Objects.equals(list, that.list);
    }

    @Override
    public int hashCode() {
      return Objects.hash(list);
    }

    @Override
    public String toString() {
      return "ListAds{" +
          "list=" + list +
          '}';
    }
  }
}
