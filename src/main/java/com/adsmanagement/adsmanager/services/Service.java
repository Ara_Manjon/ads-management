package com.adsmanagement.adsmanager.services;


import com.adsmanagement.adsmanager.domain.Manager;

public abstract class Service {

  protected Manager managerService;

  public Service(Manager managerService) {
    this.managerService = managerService;
  }
}
