package com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects;

import java.sql.Timestamp;
import java.util.Objects;

public class Date {
  private final Timestamp date;

  public Date(Timestamp date) {
    this.date = date;
  }

  public Timestamp get() {
    return this.date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Date date1 = (Date) o;
    return Objects.equals(date, date1.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date);
  }

  @Override
  public String toString() {
    return "Date{" +
        "date=" + date +
        '}';
  }
}
