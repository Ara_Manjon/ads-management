package com.adsmanagement.adsmanager.domain.entities.ad.domain.entities;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.TitleAndDescriptionCanNotBeTheSame;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Identifier;

import java.util.Objects;

public class Ad {

  private final Identifier id;
  private final Title title;
  private final Description description;
  private final Date date;

  public Ad(AdBuilder builder) {
    this.id = builder.id;
    this.title = builder.title;
    this.description = builder.description;
    this.date = builder.date;
  }

  public Identifier getIdentifier() {
    return this.id;
  }

  public static class AdBuilder {
    private Identifier id;
    private Title title;
    private Description description;
    private Date date;

    public AdBuilder id(Identifier identifier) {
      this.id = identifier;
      return this;
    }

    public AdBuilder title(Title title) {
      this.title = title;
      return this;
    }

    public AdBuilder description(Description description) {
      this.description = checkDescription(description);
      return this;
    }

    private Description checkDescription(Description description) {
      if (description.get().equals(this.title.get())) throw new TitleAndDescriptionCanNotBeTheSame();
      return description;
    }

    public AdBuilder date(Date date) {
      this.date = date;
      return this;
    }

    public Ad build() {
      return new Ad(this);
    }
  }

  public static Ad createFromDTO(DTO.Ad adDTO) {
    return new AdBuilder()
        .id(new Id(adDTO.id))
        .title(new Title(adDTO.title))
        .description(new Description(adDTO.description))
        .date(new Date(adDTO.date))
        .build();
  }

  public DTO.Ad createDTO() {
    DTO.Ad adDto = new DTO.Ad();
    adDto.id = this.id.get();
    adDto.title = this.title.get();
    adDto.description = this.description.get();
    adDto.date = this.date.get();
    return adDto;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Ad ad = (Ad) o;
    return Objects.equals(id, ad.id) &&
        Objects.equals(title, ad.title) &&
        Objects.equals(description, ad.description) &&
        Objects.equals(date, ad.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, title, description, date);
  }

  @Override
  public String toString() {
    return "Ad{" +
        "id=" + id +
        ", title=" + title +
        ", description=" + description +
        ", date=" + date +
        '}';
  }
}
