package com.adsmanagement.adsmanager.domain.entities.ad.infrastructure.inMemory;

import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Identifier;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.Capacity;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.SorterByDate;


public class Catalog implements AdRepository {

  private final ListAds listAds = new ListAds();
  private final Capacity capacity = Capacity.HIGH;

  @Override
  public void create(Ad ad) {
    checkStockCapacity();
    listAds.insert(ad);
  }

  @Override
  public Ad findBy(AdUseCase adUseCase) {
    return listAds.selectBy(adUseCase.title, adUseCase.description);
  }

  @Override
  public int delete(Identifier identifier) {
    return listAds.delete(identifier.get());
  }

  @Override
  public ListAds findAll() {
    listAds.sort();
    return listAds;
  }

  private void checkStockCapacity() {
    while (this.listAds.size() >= capacity.getValue()) {
      Ad ad = listAds.retrieveLast(new SorterByDate());
      delete(ad.getIdentifier());
    }
  }

  @Override
  public void expireBy(Date expirationDate) {
    listAds.deleteOldestThan(expirationDate);
  }

}
