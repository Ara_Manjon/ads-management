package com.adsmanagement.adsmanager.domain.entities.ad.controllers;

import com.adsmanagement.manager.managerservices.ManagerServices;
import com.adsmanagement.manager.managerservices.infrastructure.Repository;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.AdService;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.AdException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AdServiceController {

  @Autowired
  Repository SQLAdRepository;
  //IRepository catalog;

  @Autowired
  GeneratorID generatorID;

  @Autowired
  TimeServer timeServer;

  @PostMapping(value = "ads", consumes = "application/json", produces = "application/json")
  public ResponseEntity<Object> addAd(@RequestBody AdUseCase ad) {
    try {
      AdService adService = new AdService(new ManagerServices(SQLAdRepository, generatorID, timeServer));
      adService.add(ad);
      return new ResponseEntity<>("Ad added successfully", HttpStatus.CREATED);
    } catch (AdException error) {
      return new ResponseEntity<>(error.getMessage(), HttpStatus.BAD_REQUEST);
    } catch (Exception error) {
      return new ResponseEntity<>("Service Unavailable", HttpStatus.SERVICE_UNAVAILABLE);
    }
  }

  @GetMapping(value = "ads", produces = "application/json")
  public ResponseEntity<Object> listAds() {
    try {
      AdService adService = new AdService(new ManagerServices(SQLAdRepository, generatorID, timeServer));
      return new ResponseEntity<>(adService.list(), HttpStatus.OK);
    } catch (AdException error) {
      return new ResponseEntity<>(error.getMessage(), HttpStatus.BAD_REQUEST);
    } catch (Exception error) {
      return new ResponseEntity<>("Service Unavailable", HttpStatus.SERVICE_UNAVAILABLE);
    }
  }

  @DeleteMapping(value = "ads/{adId}", produces = "application/json")
  public ResponseEntity<Object> removeAd(@PathVariable String adId) {
    try {
      AdService adService = new AdService(new ManagerServices(SQLAdRepository, generatorID, timeServer));
      adService.remove(new Id(adId));
      return new ResponseEntity<>("Ad removed successfully", HttpStatus.OK);
    } catch (AdException error) {
      return new ResponseEntity<>(error.getMessage(), HttpStatus.BAD_REQUEST);
    } catch (Exception error) {
      return new ResponseEntity<>("Service Unavailable", HttpStatus.SERVICE_UNAVAILABLE);
    }
  }
}
