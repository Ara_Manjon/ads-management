package com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions;

public class TitleCannotBeBiggerThan50 extends AdException {
  public TitleCannotBeBiggerThan50() {
    super("Title can not be bigger than 50 characters");
  }
}
