package com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions;

public class AdNotFound extends AdException {
  public AdNotFound() {
    super("Ad not found");
  }
}
