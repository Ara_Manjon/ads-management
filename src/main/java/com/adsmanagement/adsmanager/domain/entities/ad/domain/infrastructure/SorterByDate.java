package com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;

import java.util.Comparator;

public class SorterByDate implements Comparator<Ad> {

  public int compare(Ad a, Ad b){
    return b.createDTO().date.compareTo(a.createDTO().date);
  }

  @Override
  public boolean equals(Object o) {
    return super.equals(o);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
