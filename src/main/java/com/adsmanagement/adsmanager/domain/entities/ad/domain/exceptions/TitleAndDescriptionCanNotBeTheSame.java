package com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions;

public class TitleAndDescriptionCanNotBeTheSame extends AdException {

  public TitleAndDescriptionCanNotBeTheSame() {
    super("Title and Description can not be the same");
  }
}
