package com.adsmanagement.adsmanager.domain.entities.ad.domain.entities;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.SorterByDate;
import com.adsmanagement.adsmanager.infrastructure.DTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ListAds {

  private List<Ad> list = new ArrayList<>();
  private final SorterByDate sorter = new SorterByDate();

  public void sort() {
    list.sort(sorter);
  }

  public void insert(Ad ad) {
    list.add(ad);
  }

  public Ad selectBy(String title, String description) {

    for (Ad ad : list) {
      DTO.Ad adDTO = ad.createDTO();
      if (adDTO.title.equals(title) && adDTO.description.equals(description))
        return ad;
    }
    return null;
  }

  public int delete(String identifier) {
    for (Ad ad : list) {
      DTO.Ad adDTO = ad.createDTO();
      if (adDTO.id.equals(identifier)) {

        list.remove(ad);
        return 1;
      }
    }
    return 0;
  }

  public void deleteOldestThan(Date expirationDate) {
    List<Ad> newList = new ArrayList<>();
    for (Ad ad : list) {
      DTO.Ad adDTO = ad.createDTO();
      if (adDTO.date.compareTo(expirationDate.get()) >= 0) newList.add(ad);
    }
    list = newList;
  }

  public Ad retrieveLast(SorterByDate sorterByDate) {
    list.sort(sorterByDate);
    return list.get(list.size()-1);
  }

  public Ad get(int index) {
    return list.get(index);
  }

  public int size() {
    return list.size();
  }

  public static ListAds createFromDTO(DTO.ListAds listAdsDTO) {
    ListAds listAds = new ListAds();
    for (DTO.Ad adDTO : listAdsDTO) {
      listAds.list.add(Ad.createFromDTO(adDTO));
    }
    return listAds;
  }

  public DTO.ListAds createDTO() {
    DTO.ListAds listAds = new DTO.ListAds();
    for (Ad ad : list) {
      listAds.add(ad.createDTO());
    }
    return listAds;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ListAds listAds = (ListAds) o;
    return Objects.equals(list, listAds.list) &&
        Objects.equals(sorter.getClass(), listAds.sorter.getClass());
  }

  @Override
  public int hashCode() {
    return Objects.hash(list, sorter);
  }
}
