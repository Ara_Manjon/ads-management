package com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.TitleCannotBeBiggerThan50;

import java.util.Objects;

public class Title {
  private final String title;

  public Title(String title) {
    if (title.length() > 50) throw new TitleCannotBeBiggerThan50();
    this.title = title;
  }

  public String get() {
    return this.title;
  }

  @Override
  public boolean equals(Object o) {

    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Title title1 = (Title) o;
    return Objects.equals(title, title1.title);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title);
  }

}
