package com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure;

import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Identifier;
import com.adsmanagement.manager.managerservices.infrastructure.Repository;

public interface AdRepository extends Repository {

  void create(Ad ad);

  Ad findBy(AdUseCase ad);

  int delete(Identifier identifier);

  ListAds findAll();

  void expireBy(Date expirationDate);
}
