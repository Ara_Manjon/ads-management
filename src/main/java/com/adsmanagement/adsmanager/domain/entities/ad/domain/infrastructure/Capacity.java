package com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure;

public enum Capacity {
  HIGH("high",100);

  private final int value;
  private final String name;

  Capacity(String name, int value) {
    this.name = name;
    this.value = value;
  }

  public int getValue(){
    return this.value;
  }
}
