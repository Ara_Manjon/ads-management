package com.adsmanagement.adsmanager.domain.entities.ad.controllers;


import com.adsmanagement.adsmanager.infrastructure.UseCase;

public class AdUseCase implements UseCase {
  public String title;
  public String description;
}

