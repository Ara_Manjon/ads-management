package com.adsmanagement.adsmanager.domain.entities.ad.domain;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.adsmanager.services.Service;
import com.adsmanagement.adsmanager.domain.Manager;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Identifier;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.AdNotFound;

public class AdService extends Service {

  private final AdRepository catalog;

  public AdService(Manager managerService) {
    super(managerService);
    this.catalog = (AdRepository) super.managerService.generateRepository();
  }

  public Identifier generateId() {
    return super.managerService.generateId();
  }

  public Date generateDate() {
    return super.managerService.generateDate();
  }

  public void add(AdUseCase adUseCase) {
    Ad adFound = this.catalog.findBy(adUseCase);

    if (adFound != null) saveUpdatedAd(updateAd(adUseCase, adFound));
    else this.catalog.create(createAd(adUseCase));
  }

  public void remove(Identifier advertId) {
    if (this.catalog.delete(advertId) == 0) throw new AdNotFound();
  }

  public DTO.ListAds list() {
    ListAds listAds = this.catalog.findAll();

    return listAds.createDTO();
  }

  private void saveUpdatedAd(Ad ad) {
    this.catalog.delete(ad.getIdentifier());
    this.catalog.create(ad);
  }

  private Ad createAd(AdUseCase adUseCase) {
    return new Ad.AdBuilder()
        .id(generateId())
        .title(new Title(adUseCase.title))
        .description(new Description(adUseCase.description))
        .date(generateDate())
        .build();
  }

  private Ad updateAd(AdUseCase adUseCase, Ad adFound) {
    DTO.Ad adDTO = adFound.createDTO();
    return new Ad.AdBuilder()
        .id(new Id(adDTO.id))
        .title(new Title(adUseCase.title))
        .description(new Description(adUseCase.description))
        .date(generateDate())
        .build();
  }
}
