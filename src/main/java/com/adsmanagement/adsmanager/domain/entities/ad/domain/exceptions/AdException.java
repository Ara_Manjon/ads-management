package com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions;

public class AdException extends RuntimeException {

  public AdException(String message) {
    super(message);
  }
}
