package com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier;

public interface Identifier {
  String get();
}
