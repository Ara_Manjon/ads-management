package com.adsmanagement.adsmanager.domain.entities.ad.infrastructure.sql;

import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Identifier;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.Capacity;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.SorterByDate;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

@Primary
@Component
public class SQLAdRepository implements AdRepository {

  private final Capacity capacity = Capacity.HIGH;

  @Autowired
  JdbcTemplate jdbcTemplate;

  public void create(Ad ad) {

    DTO.Ad adDTO = ad.createDTO();
    checkStockCapacity();
    jdbcTemplate.update("INSERT INTO ads (id, title, description, ad_date) VALUES (?, ?, ?, ?)", adDTO.id, adDTO.title, adDTO.description, adDTO.date);
  }

  @Override
  public Ad findBy(AdUseCase ad) {
    try {
      DTO.Ad adFound = new DTO.Ad();
      jdbcTemplate.queryForObject("SELECT * FROM ads WHERE title=? AND description=?",
          new Object[]{ad.title, ad.description},
          (rs, rowNum) -> {
            adFound.id = rs.getString("id");
            adFound.title = rs.getString("title");
            adFound.description = rs.getString("description");
            adFound.date = rs.getTimestamp("ad_date");
            return null;
          });

      Objects.requireNonNull(adFound.id);
      return Optional.of(Ad.createFromDTO(adFound)).orElse(null);

    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public int delete(Identifier identifier) {
    return jdbcTemplate.update("DELETE FROM ads WHERE id=?", identifier.get());
  }

  @Override
  public ListAds findAll() {
    DTO.ListAds listAds = new DTO.ListAds();
    try {
      jdbcTemplate.query("SELECT * FROM ads ORDER BY ad_date DESC",
          (rs, rowNum) -> {
            DTO.Ad adDTO = new DTO.Ad();
            adDTO.id = rs.getString("id");
            adDTO.title = rs.getString("title");
            adDTO.description = rs.getString("description");
            adDTO.date = rs.getTimestamp("ad_date");
            listAds.add(adDTO);
            return null;
          });

      return Optional.of(ListAds.createFromDTO(listAds)).orElse(null);

    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public void expireBy(Date expirationDate) {
    jdbcTemplate.update("DELETE FROM ads WHERE ad_date<?", expirationDate.get());
  }

  private void checkStockCapacity() {

    int value = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ads", Integer.class);
    if (value == capacity.getValue()) {
      ListAds listAds = findAll();
      Ad ad = listAds.retrieveLast(new SorterByDate());
      delete(ad.getIdentifier());

    }
  }
}
