package com.adsmanagement.adsmanager.domain;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Identifier;
import com.adsmanagement.manager.managerservices.infrastructure.Repository;

public interface Manager {

  Identifier generateId();

  Date generateDate();

  Repository generateRepository();

}
