package com.adsmanagement.adsmanager.domain.exceptions;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.AdException;

public class KeyValueDuplicated extends AdException {
  public KeyValueDuplicated() {
    super("Key value duplicated");
  }
}
