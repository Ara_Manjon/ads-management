package com.adsmanagement.adsmanager.domain.expirator.timeconverter;


import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TimestampConverter {

  public static Timestamp convert(String dateString) {

    try {
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
      DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      String dateFormatted = LocalDate.parse(dateString, formatter).format(formatter2);

      LocalDate lc = LocalDate.parse(dateFormatted);
      return Timestamp.valueOf(lc.atStartOfDay());
    } catch (Exception e) {
      throw new NotValidFormatDate();
    }
  }
}
