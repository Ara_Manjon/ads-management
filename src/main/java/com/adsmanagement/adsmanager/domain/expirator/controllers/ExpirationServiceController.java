package com.adsmanagement.adsmanager.domain.expirator.controllers;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.AdException;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.adsmanager.domain.expirator.ExpirationService;
import com.adsmanagement.manager.managerservices.ManagerServices;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.Repository;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExpirationServiceController {

  @Autowired
  Repository SQLAdRepository;
  //IRepository catalog;

  @Autowired
  GeneratorID generatorID;

  @Autowired
  TimeServer timeServer;

  @PatchMapping(value = "ads", consumes = "application/json", produces = "application/json")
  public ResponseEntity<Object> expireAds( @RequestBody ExpirationDateUseCase expirationDate) {
    try {
      ExpirationService advertService = new ExpirationService(new ManagerServices(SQLAdRepository, generatorID, timeServer));
      advertService.expire(new Date(TimestampConverter.convert(expirationDate.date)));
      return new ResponseEntity<>("Ads expired successfully", HttpStatus.OK);
    } catch (AdException error) {
      return new ResponseEntity<>(error.getMessage(), HttpStatus.BAD_REQUEST);
    } catch (Exception error) {
      return new ResponseEntity<>("Service Unavailable", HttpStatus.SERVICE_UNAVAILABLE);
    }
  }
}
