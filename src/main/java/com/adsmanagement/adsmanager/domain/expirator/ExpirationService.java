package com.adsmanagement.adsmanager.domain.expirator;

import com.adsmanagement.adsmanager.services.Service;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.manager.managerservices.ManagerServices;

public class ExpirationService extends Service {

  private final AdRepository repository;

  public ExpirationService(ManagerServices managerService) {
    super(managerService);
    this.repository = (AdRepository) super.managerService.generateRepository();
  }

  public void expire(Date expirationDate){
    this.repository.expireBy(expirationDate);
  }

}
