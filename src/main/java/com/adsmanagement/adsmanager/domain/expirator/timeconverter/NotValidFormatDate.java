package com.adsmanagement.adsmanager.domain.expirator.timeconverter;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.AdException;

public class NotValidFormatDate extends AdException {
  public NotValidFormatDate() {
    super("Date should has format dd-mm-yyyy");
  }
}
