package com.adsmanagement.integration;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.entities.ad.infrastructure.sql.SQLAdRepository;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.services.GeneratorUUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.sql.Timestamp;
import java.util.Calendar;

@SpringBootTest
@Sql(scripts = {"/schema.sql"})
@Sql(scripts = {"/test-cleaner.sql"})
public class SQLAdRepositoryShould {

  @Autowired
  SQLAdRepository sqlAdRepository;

  GeneratorID generatorUUID = new GeneratorUUID();

  @Test
  public void create_and_delete_an_ad_correctly() {

    Id id = new Id(generatorUUID.generate());
    Date date = new Date(TimestampConverter.convert("24-04-2020"));
    Ad ad = new Ad.AdBuilder()
        .id(id)
        .title(new Title("title"))
        .description(new Description("description"))
        .date(date)
        .build();

    sqlAdRepository.create(ad);

    Assertions.assertEquals(1, sqlAdRepository.delete(ad.getIdentifier()));
  }

  @Test
  public void ad_not_found_to_delete() {

    Id id = new Id(generatorUUID.generate());

    Assertions.assertEquals(0, sqlAdRepository.delete(id));
  }

  @Test
  public void find_ad_by_content_correctly() {

    Id id = new Id(generatorUUID.generate());
    Date date = new Date(TimestampConverter.convert("24-04-2020"));
    Ad ad = new Ad.AdBuilder()
        .id(id)
        .title(new Title("title"))
        .description(new Description("description"))
        .date(date)
        .build();
    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "title";
    adUseCase.description = "description";
    sqlAdRepository.create(ad);

    Assertions.assertEquals(ad, sqlAdRepository.findBy(adUseCase));
  }

  @Test
  public void ad_not_found_by_content() {

    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "title";
    adUseCase.description = "description";

    Assertions.assertNull(sqlAdRepository.findBy(adUseCase));
  }

  @Test
  public void retrieve_all_ads_correctly_() {

    Id id = new Id(generatorUUID.generate());
    Id id2 = new Id(generatorUUID.generate());
    Ad ad = new Ad.AdBuilder()
        .id(id)
        .title(new Title("title"))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("01-04-2020")))
        .build();
    Ad ad2 = new Ad.AdBuilder()
        .id(id2)
        .title(new Title("title2"))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("02-04-2020")))
        .build();
    DTO.ListAds listAds = new DTO.ListAds();
    listAds.add(ad2.createDTO());
    listAds.add(ad.createDTO());

    sqlAdRepository.create(ad);
    sqlAdRepository.create(ad2);

    Assertions.assertEquals(ListAds.createFromDTO(listAds), sqlAdRepository.findAll());
  }

  @Test
  public void expire_a_catalog_by_date_correctly() {

    GeneratorID generatorID = new GeneratorUUID();
    Timestamp time = TimestampConverter.convert("01-01-2020");
    Timestamp date = TimestampConverter.convert("27-02-2020");
    Date expirationDate = new Date(date);
    Calendar cal = Calendar.getInstance();
    ListAds listAds = new ListAds();
    for (int i = 1; i < 60; i++) {
      cal.setTime(time);
      cal.add(Calendar.DAY_OF_WEEK, 1);
      time = new Timestamp(cal.getTime().getTime());
      Ad ad = new Ad.AdBuilder()
          .id(new Id(i + generatorID.generate()))
          .title(new Title("title" + i))
          .description(new Description("description" + i))
          .date(new Date(time))
          .build();
      sqlAdRepository.create(ad);
      if (time.compareTo(date) >= 0) listAds.insert(ad);
    }
    listAds.sort();

    sqlAdRepository.expireBy(expirationDate);

    Assertions.assertEquals(listAds, sqlAdRepository.findAll());
  }

  @Test
  public void delete_oldest_ad_when_capacity_is_overcome_when_add_a_new_ad(){

    Timestamp time = TimestampConverter.convert("01-01-2020");
    Calendar calendar = Calendar.getInstance();
    ListAds listAds = new ListAds();
    for (int i = 1; i < 101; i++) {
      calendar.setTime(time);
      calendar.add(Calendar.DAY_OF_WEEK, 1);
      time = new Timestamp(calendar.getTime().getTime());
      Ad ad = new Ad.AdBuilder()
          .id(new Id(i + generatorUUID.generate()))
          .title(new Title("title" + i))
          .description(new Description("description" + i))
          .date(new Date(time))
          .build();
      sqlAdRepository.create(ad);
      listAds.insert(ad);
    }
    Ad newAd = new Ad.AdBuilder()
        .id(new Id("newAd"))
        .title(new Title("title-new-ad"))
        .description(new Description("description-new-ad"))
        .date(new Date(TimestampConverter.convert("24-04-2020")))
        .build();
    listAds.sort();
    DTO.ListAds listExpected = listAds.createDTO();
    listExpected.list.remove(99);
    listExpected.list.add(0,newAd.createDTO());

    sqlAdRepository.create(newAd);

    Assertions.assertEquals(ListAds.createFromDTO(listExpected), sqlAdRepository.findAll());
  }
}
