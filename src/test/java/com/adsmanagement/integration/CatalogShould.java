package com.adsmanagement.integration;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.entities.ad.infrastructure.inMemory.Catalog;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.services.GeneratorUUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.Calendar;

public class CatalogShould {

  @Test
  public void delete_oldest_ad_when_capacity_is_overcome_when_add_a_new_ad() {

    ListAds listAds = new ListAds();
    Timestamp time = TimestampConverter.convert("01-01-2020");
    Calendar calendar = Calendar.getInstance();
    GeneratorID generator = new GeneratorUUID();
    Catalog catalog = new Catalog();
    for (int i = 1; i < 101; i++) {
      calendar.setTime(time);
      calendar.add(Calendar.DAY_OF_WEEK, 1);
      time = new Timestamp(calendar.getTime().getTime());
      Ad ad = new Ad.AdBuilder()
          .id(new Id(i + generator.generate()))
          .title(new Title("title" + i))
          .description(new Description("description" + i))
          .date(new Date(time))
          .build();
      catalog.create(ad);
      listAds.insert(ad);
    }
    Ad newAd = new Ad.AdBuilder()
        .id(new Id("newAd"))
        .title(new Title("title-new-ad"))
        .description(new Description("description-new-ad"))
        .date(new Date(TimestampConverter.convert("24-04-2020")))
        .build();
    DTO.ListAds listExpectedDTO = listAds.createDTO();
    DTO.Ad lastAd = listExpectedDTO.list.get(0);
    listExpectedDTO.list.remove(lastAd);
    listExpectedDTO.add(newAd.createDTO());
    ListAds listAdsExpected = ListAds.createFromDTO(listExpectedDTO);
    listAdsExpected.sort();

    catalog.create(newAd);

    Assertions.assertEquals(listAdsExpected, catalog.findAll());
  }
}
