package com.adsmanagement.acceptance;

import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.entities.ad.infrastructure.sql.SQLAdRepository;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.AdService;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.manager.managerservices.ManagerServices;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import com.adsmanagement.manager.services.GeneratorUUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.sql.Timestamp;
import java.util.Calendar;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@Sql(scripts = {"/schema.sql"})
@Sql(scripts = {"/test-cleaner.sql"})
public class RemoveOldestAdWhenMaximumSizeHasBeenReached {

  @Mock
  GeneratorID generatorID;

  @Mock
  TimeServer timeServer;

  @Autowired
  AdRepository catalog = new SQLAdRepository();
  //AdRepository catalog = new Catalog();

  ListAds listAds = new ListAds();

  @BeforeEach
  public void setup() {

    MockitoAnnotations.initMocks(this);
    Timestamp time = TimestampConverter.convert("01-01-2020");
    Calendar calendar = Calendar.getInstance();
    GeneratorID generator = new GeneratorUUID();
    for (int i = 1; i < 101; i++) {
      calendar.setTime(time);
      calendar.add(Calendar.DAY_OF_WEEK, 1);
      time = new Timestamp(calendar.getTime().getTime());
      Ad ad = new Ad.AdBuilder()
          .id(new Id(i + generator.generate()))
          .title(new Title("title" + i))
          .description(new Description("description" + i))
          .date(new Date(time))
          .build();
      catalog.create(ad);
      listAds.insert(ad);
    }
  }

  @Test
  public void remove_oldest_ad_when_maximum_size_has_been_reached() {

    AdService adService = new AdService(new ManagerServices(catalog, generatorID, timeServer));
    Ad newAd = new Ad.AdBuilder()
        .id(new Id("newAd"))
        .title(new Title("title-new-ad"))
        .description(new Description("description-new-ad"))
        .date(new Date(TimestampConverter.convert("24-04-2020")))
        .build();
    listAds.sort();
    DTO.ListAds listExpectedDTO = listAds.createDTO();
    listExpectedDTO.list.remove(99);
    listExpectedDTO.list.add(0, newAd.createDTO());
    AdUseCase useCaseAd = new AdUseCase();
    useCaseAd.title = "title-new-ad";
    useCaseAd.description = "description-new-ad";
    when(timeServer.getDate()).thenReturn(TimestampConverter.convert("24-04-2020"));
    when(generatorID.generate()).thenReturn("newAd");

    adService.add(useCaseAd);

    Assertions.assertEquals(listExpectedDTO, adService.list());
  }
}
