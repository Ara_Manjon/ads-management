package com.adsmanagement.acceptance;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.manager.managerservices.ManagerServices;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.AdService;
import com.adsmanagement.adsmanager.domain.expirator.ExpirationService;
import com.adsmanagement.adsmanager.domain.entities.ad.infrastructure.inMemory.Catalog;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import com.adsmanagement.manager.services.GeneratorUUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.Calendar;

@ExtendWith(MockitoExtension.class)
public class RemoveListAdsPublishedBeforeAGivenDate {

  @Mock
  TimeServer timeServer;

  @Mock
  GeneratorID generatorID;

  AdRepository catalog = new Catalog();

  ListAds listAds = new ListAds();

  @BeforeEach
  public void setup() {

    MockitoAnnotations.initMocks(this);

    GeneratorID generatorUUID = new GeneratorUUID();
    Timestamp time = TimestampConverter.convert("01-01-2020");
    Calendar calendar = Calendar.getInstance();
    for (int i = 1; i < 60; i++) {
      calendar.setTime(time);
      calendar.add(Calendar.DAY_OF_WEEK, 1);
      time = new Timestamp(calendar.getTime().getTime());
      Ad ad = new Ad.AdBuilder()
          .id(new Id(i+generatorUUID.generate()))
          .title(new Title("title" + i))
          .description(new Description("description" + i))
          .date(new Date(time))
          .build();
      catalog.create(ad);
      listAds.insert(ad);
    }
  }

  @Test
  public void remove_ads_published_before_a_given_date() {

    ManagerServices managerServices = new ManagerServices(catalog, generatorID, timeServer);
    AdService adService = new AdService(managerServices);
    ExpirationService expirationService = new ExpirationService(managerServices);
    Ad lastAd = listAds.get(listAds.size()-1);
    DTO.ListAds listExpectedDTO = new DTO.ListAds();
    listExpectedDTO.add(lastAd.createDTO());
    Date expirationDate = new Date(TimestampConverter.convert("29-02-2020"));

    expirationService.expire(expirationDate);

    Assertions.assertEquals(listExpectedDTO, adService.list());
  }
}
