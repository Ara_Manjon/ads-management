package com.adsmanagement.acceptance;

import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.AdService;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.manager.managerservices.ManagerServices;
import com.adsmanagement.adsmanager.domain.entities.ad.infrastructure.inMemory.Catalog;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@Sql(scripts = {"/schema.sql"})
@Sql(scripts = {"/test-cleaner.sql"})
public class AddRemoveAndListAdsFromCatalog {

  @Mock
  GeneratorID generatorID;

  @Mock
  TimeServer timeServer;

  //@Autowired
  //AdRepository catalog = new SQLAdRepository();
  AdRepository catalog = new Catalog();

  @BeforeEach
  public void setup() {

    MockitoAnnotations.initMocks(this);

    Ad ad = new Ad.AdBuilder()
        .id(new Id("adInCatalog"))
        .date(new Date(TimestampConverter.convert( "24-04-2020")))
        .title(new Title("title"))
        .description(new Description("description"))
        .build();

    catalog.create(ad);
  }

  @Test
  public void add_remove_and_list_ads_from_catalog(){

    AdService adService = new AdService(new ManagerServices(catalog, generatorID, timeServer));
    AdUseCase adToAdd = new AdUseCase();
    adToAdd.title = "title";
    adToAdd.description = "description";
    AdUseCase adToAdd2 = new AdUseCase();
    adToAdd2.title = "title2";
    adToAdd2.description = "description2";
    Ad adExpected = new Ad.AdBuilder()
        .id(new Id("adInCatalog"))
        .date(new Date(TimestampConverter.convert( "25-04-2020")))
        .title(new Title("title"))
        .description(new Description("description"))
        .build();
    DTO.ListAds listExpectedDTO = new DTO.ListAds();
    listExpectedDTO.list.add(adExpected.createDTO());
    when(timeServer.getDate()).thenReturn(TimestampConverter.convert("25-04-2020"), TimestampConverter.convert("26-04-2020"));
    when(generatorID.generate()).thenReturn("idToRemove");

    adService.add(adToAdd);
    adService.add(adToAdd2);
    adService.remove(new Id("idToRemove"));

    Assertions.assertEquals(listExpectedDTO, adService.list());
  }
}
