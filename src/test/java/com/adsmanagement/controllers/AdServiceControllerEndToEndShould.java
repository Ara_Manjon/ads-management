package com.adsmanagement.controllers;

import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.adsmanagement.manager.services.GeneratorUUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = {"/test-cleaner.sql"})
public class AdServiceControllerEndToEndShould {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private AdRepository catalog;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private GeneratorID generatorID;

  @MockBean
  private TimeServer timeServer;

  GeneratorID generator = new GeneratorUUID();
  String id1 = generator.generate();
  String id2 = generator.generate();

  @BeforeEach
  public void setupCreateCatalogWithAds() throws Exception {

    MockitoAnnotations.initMocks(this);

    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "title" + this.id1;
    adUseCase.description = "description";

    AdUseCase adUseCase2 = new AdUseCase();
    adUseCase2.title = "title" + this.id2;
    adUseCase2.description = "description";

    when(generatorID.generate()).thenReturn(this.id1, this.id2);
    when(timeServer.getDate()).thenReturn(TimestampConverter.convert("21-04-2020"), TimestampConverter.convert("22-04-2020"));

    this.mockMvc.perform(post("/ads").contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(adUseCase)));

    this.mockMvc.perform(post("/ads").contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(adUseCase2)));
  }

  @Test
  public void add_ad_correctly() throws Exception {

    String newAdId = generator.generate();
    AdUseCase ad = new AdUseCase();
    ad.title = "title" + newAdId;
    ad.description = "description";
    when(generatorID.generate()).thenReturn(newAdId);
    when(timeServer.getDate()).thenReturn(TimestampConverter.convert("21-04-2020"));

    String jsonRequest = new ObjectMapper().writeValueAsString(ad);
    MvcResult result = this.mockMvc.perform(post("/ads").contentType(MediaType.APPLICATION_JSON)
        .content(jsonRequest)).andDo(print()).andExpect(status().isCreated())
        .andReturn();
    String resultAsString = result.getResponse().getContentAsString();

    Assertions.assertEquals("Ad added successfully", resultAsString);
  }

  @Test
  public void remove_ad_correctly() throws Exception {

    MvcResult result = this.mockMvc.perform(delete("/ads/" + this.id1))
        .andDo(print()).andExpect(status().is(200))
        .andReturn();
    String resultAsString = result.getResponse().getContentAsString();

    Assertions.assertEquals("Ad removed successfully", resultAsString);
  }

  @Test
  public void list_ads_correctly() throws Exception {

    DTO.ListAds listExpectedDTO = new DTO.ListAds();
    Ad ad = new Ad.AdBuilder()
        .id(new Id(this.id1))
        .title(new Title("title" + this.id1))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("21-04-2020")))
        .build();
    Ad ad2 = new Ad.AdBuilder()
        .id(new Id(this.id2))
        .title(new Title("title" + this.id2))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("22-04-2020")))
        .build();
    listExpectedDTO.list.add(ad2.createDTO());
    listExpectedDTO.list.add(ad.createDTO());

    MvcResult result = this.mockMvc.perform(get("/ads"))
        .andDo(print()).andExpect(status().is(200))
        .andReturn();
    String resultAsString = result.getResponse().getContentAsString();
    DTO.ListAds adResult = objectMapper.readValue(resultAsString, DTO.ListAds.class);

    Assertions.assertEquals(listExpectedDTO, adResult);
  }
}
