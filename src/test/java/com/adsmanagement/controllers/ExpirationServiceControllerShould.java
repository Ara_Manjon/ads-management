package com.adsmanagement.controllers;

import com.adsmanagement.adsmanager.domain.expirator.controllers.ExpirationDateUseCase;
import com.adsmanagement.adsmanager.domain.expirator.controllers.ExpirationServiceController;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class ExpirationServiceControllerShould {

  @Mock
  private AdRepository catalog;

  @Mock
  private GeneratorID idGenerator;

  @Mock
  private TimeServer timeServer;

  @InjectMocks
  private ExpirationServiceController expirationServiceController;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void expire_ads_by_date_correctly() {
    ExpirationDateUseCase expirationDate= new ExpirationDateUseCase();
    expirationDate.date = "24-04-2020";

    ResponseEntity<Object> response = expirationServiceController.expireAds(expirationDate);

    Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    Assertions.assertEquals("Ads expired successfully", response.getBody());
  }

  @Test
  public void throw_an_error_when_date_introduced_not_has_valid_format() {
    ExpirationDateUseCase expirationDate= new ExpirationDateUseCase();
    expirationDate.date = "2-04-2020";

    ResponseEntity<Object> response = expirationServiceController.expireAds(expirationDate);

    Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    Assertions.assertEquals("Date should has format dd-mm-yyyy", response.getBody());
  }
}
