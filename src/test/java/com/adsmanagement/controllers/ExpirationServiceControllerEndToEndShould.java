package com.adsmanagement.controllers;

import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import com.adsmanagement.adsmanager.domain.expirator.controllers.ExpirationDateUseCase;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.adsmanagement.manager.services.GeneratorUUID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = {"/test-cleaner.sql"})
public class ExpirationServiceControllerEndToEndShould {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private AdRepository catalogRepository;

  @MockBean
  private GeneratorID generatorID;

  @MockBean
  private TimeServer timeServer;

  @Autowired
  private ObjectMapper objectMapper;

  GeneratorID generator = new GeneratorUUID();
  String id1 = generator.generate();
  String id2 = generator.generate();
  String id3 = generator.generate();

  @BeforeEach
  public void setup() throws Exception {

    MockitoAnnotations.initMocks(this);

    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "title" + this.id1;
    adUseCase.description = "description";

    AdUseCase adUseCase2 = new AdUseCase();
    adUseCase2.title = "title" + this.id2;
    adUseCase2.description = "description";

    AdUseCase adUseCase3 = new AdUseCase();
    adUseCase3.title = "title" + this.id3;
    adUseCase3.description = "description";

    when(generatorID.generate()).thenReturn(this.id1, this.id2, this.id3);
    when(timeServer.getDate()).thenReturn(TimestampConverter.convert("21-04-2020"), TimestampConverter.convert("22-04-2020"), TimestampConverter.convert("23-04-2020"));

    this.mockMvc.perform(post("/ads").contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(adUseCase)));

    this.mockMvc.perform(post("/ads").contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(adUseCase2)));

    this.mockMvc.perform(post("/ads").contentType(MediaType.APPLICATION_JSON)
        .content(new ObjectMapper().writeValueAsString(adUseCase3)));

  }

  @Test
  public void expire_ads_correctly() throws Exception {

    ExpirationDateUseCase expirationDate = new ExpirationDateUseCase();
    expirationDate.date = "22-04-2020";
    String jsonRequest = new ObjectMapper().writeValueAsString(expirationDate);

    MvcResult result = this.mockMvc.perform(patch("/ads").contentType(MediaType.APPLICATION_JSON)
        .content(jsonRequest)).andDo(print()).andExpect(status().is(200))
        .andReturn();
    String resultAsString = result.getResponse().getContentAsString();

    Assertions.assertEquals("Ads expired successfully", resultAsString);
  }
}
