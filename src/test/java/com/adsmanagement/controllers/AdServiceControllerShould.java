package com.adsmanagement.controllers;

import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdServiceController;
import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.infrastructure.AdRepository;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AdServiceControllerShould {

  @Mock
  private AdRepository catalog;

  @Mock
  private GeneratorID idGenerator;

  @Mock
  private TimeServer timeServer;

  @InjectMocks
  private AdServiceController adServiceController;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }


  @Test
  public void add_an_ad_correctly() {

    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "title";
    adUseCase.description = "description";
    when(idGenerator.generate()).thenReturn("adId");
    when(timeServer.getDate()).thenReturn(TimestampConverter.convert("21-04-2020"));
    when(catalog.findBy(adUseCase)).thenReturn(null);

    ResponseEntity<Object> response = adServiceController.addAd(adUseCase);

    Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assertions.assertEquals("Ad added successfully", response.getBody());
  }

  @Test
  public void add_an_ad_correctly_when_content_exist() {

    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "title";
    adUseCase.description = "description";
    Ad ad = new Ad.AdBuilder()
        .id(new Id("adId"))
        .title(new Title("title"))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("01-04-2019")))
        .build();
    when(timeServer.getDate()).thenReturn(TimestampConverter.convert("21-04-2020"));
    when(catalog.findBy(adUseCase)).thenReturn(ad);

    ResponseEntity<Object> response = adServiceController.addAd(adUseCase);

    Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assertions.assertEquals("Ad added successfully", response.getBody());
  }

  @Test
  public void throw_an_error_if_ad_added_has_not_valid_data() {

    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "title";
    adUseCase.description = "title";
    when(catalog.findBy(adUseCase)).thenReturn(null);

    ResponseEntity<Object> response = adServiceController.addAd(adUseCase);

    Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    Assertions.assertEquals("Title and Description can not be the same", response.getBody());
  }

  @Test
  public void throw_an_error_if_ad_added_has_not_valid_title() {

    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "titleWithMoreThan50characters************************";
    adUseCase.description = "title";
    when(catalog.findBy(adUseCase)).thenReturn(null);

    ResponseEntity<Object> response = adServiceController.addAd(adUseCase);

    Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    Assertions.assertEquals("Title can not be bigger than 50 characters", response.getBody());
  }

  @Test
  public void list_ads_correctly(){

    DTO.ListAds listExpectedDTO = new DTO.ListAds();
    Ad ad = new Ad.AdBuilder()
        .id(new Id("adId"))
        .title(new Title("title"))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("01-04-2019")))
        .build();
    Ad ad2 = new Ad.AdBuilder()
        .id(new Id("adId2"))
        .title(new Title("title2"))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("02-04-2019")))
        .build();
    listExpectedDTO.list.add(ad2.createDTO());
    listExpectedDTO.list.add(ad.createDTO());
    when(catalog.findAll()).thenReturn(ListAds.createFromDTO(listExpectedDTO));

    ResponseEntity<Object> response = adServiceController.listAds();

    Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    Assertions.assertEquals(listExpectedDTO, response.getBody());
    Assertions.assertNotNull(response);
  }

  @Test
  public void remove_ad_correctly() {

    when(catalog.delete(new Id("adId"))).thenReturn(1);

    ResponseEntity<Object> response = adServiceController.removeAd("adId");

    Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    Assertions.assertEquals("Ad removed successfully", response.getBody());
  }

  @Test
  public void throw_an_error_when_ad_to_remove_is_not_found() {

    when(catalog.delete(new Id("idNotFound"))).thenReturn(0);

    ResponseEntity<Object> response = adServiceController.removeAd("idNotFound");

    Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    Assertions.assertEquals("Ad not found", response.getBody());
  }
}
