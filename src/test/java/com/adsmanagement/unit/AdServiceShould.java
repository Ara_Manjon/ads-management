package com.adsmanagement.unit;

import com.adsmanagement.adsmanager.domain.entities.ad.controllers.AdUseCase;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.manager.managerservices.ManagerServices;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.managerservices.infrastructure.TimeServer;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.infrastructure.inMemory.Catalog;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.AdService;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.AdNotFound;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AdServiceShould {

  @Mock
  private GeneratorID generatorID;

  @Mock
  private TimeServer timeServer;

  @Mock
  private Catalog catalog;

  @InjectMocks
  private ManagerServices service;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void add_an_ad_correctly() {

    AdService adService = new AdService(service);
    Id id = new Id("fakeId");
    Date date = new Date(TimestampConverter.convert("24-04-2020"));
    Ad ad = new Ad.AdBuilder()
        .id(id)
        .title(new Title("title"))
        .description(new Description("description"))
        .date(date)
        .build();
    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "title";
    adUseCase.description = "description";
    when(generatorID.generate()).thenReturn("fakeId");
    when(timeServer.getDate()).thenReturn(TimestampConverter.convert("24-04-2020"));
    when(catalog.findBy(adUseCase)).thenReturn(null);

    adService.add(adUseCase);

    verify(catalog).create(ad);
  }

  @Test
  public void add_and_update_an_ad_when_exist_content() {

    AdService adService = new AdService(service);
    Id id = new Id("adExistId");
    Date date = new Date(TimestampConverter.convert("24-04-2020"));
    Ad ad = new Ad.AdBuilder()
        .id(id)
        .title(new Title("title"))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("01-04-2020")))
        .build();
    AdUseCase adUseCase = new AdUseCase();
    adUseCase.title = "title";
    adUseCase.description = "description";
    when(timeServer.getDate()).thenReturn(TimestampConverter.convert("24-04-2020"));
    when(catalog.findBy(adUseCase)).thenReturn(ad);
    Ad adUpdated = new Ad.AdBuilder()
        .id(id)
        .title(new Title("title"))
        .description(new Description("description"))
        .date(date)
        .build();

    adService.add(adUseCase);

    verify(catalog).delete(id);
    verify(catalog).create(adUpdated);
  }

  @Test
  public void throw_an_error_when_ad_is_not_found_to_remove() {

    Id id = new Id("adToRemove");
    AdService adService = new AdService(service);
    when(catalog.delete(id)).thenReturn(0);

    Assertions.assertThrows(AdNotFound.class, () -> adService.remove(id));
  }

  @Test
  public void list_ads_ordered_by_date_correctly() {

    AdService adService = new AdService(service);
    Ad ad = new Ad.AdBuilder()
        .id(new Id("id1"))
        .title(new Title("title"))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("01-04-2020")))
        .build();
    Ad ad2 = new Ad.AdBuilder()
        .id(new Id("id2"))
        .title(new Title("title2"))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("06-04-2020")))
        .build();
    Ad ad3 = new Ad.AdBuilder()
        .id(new Id("id3"))
        .title(new Title("title3"))
        .description(new Description("description"))
        .date(new Date(TimestampConverter.convert("02-04-2020")))
        .build();
    DTO.ListAds listAds = new DTO.ListAds();
    listAds.add(ad2.createDTO());
    listAds.add(ad3.createDTO());
    listAds.add(ad.createDTO());
    ListAds listAdsExpected = ListAds.createFromDTO(listAds);
    when(catalog.findAll()).thenReturn(listAdsExpected);

    Assertions.assertEquals(listAdsExpected.createDTO(), adService.list());
  }

  @Test
  public void retrieve_empty_list_when_not_there_are_ads() {

    AdService adService = new AdService(service);
    DTO.ListAds listAds = new DTO.ListAds();

    when(catalog.findAll()).thenReturn(ListAds.createFromDTO(listAds));

    Assertions.assertEquals(listAds, adService.list());
  }
}
