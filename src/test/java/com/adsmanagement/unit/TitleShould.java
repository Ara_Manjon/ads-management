package com.adsmanagement.unit;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.TitleCannotBeBiggerThan50;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TitleShould {
  @Test
  public void throw_an_error_if_title_is_bigger_than_50_characters() {

    Assertions.assertThrows(TitleCannotBeBiggerThan50.class, ()-> new Title("******************************************************"));
  }
}
