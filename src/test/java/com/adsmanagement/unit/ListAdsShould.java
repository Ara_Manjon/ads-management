package com.adsmanagement.unit;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.ListAds;
import com.adsmanagement.adsmanager.infrastructure.DTO;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import com.adsmanagement.manager.managerservices.infrastructure.GeneratorID;
import com.adsmanagement.manager.services.GeneratorUUID;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.Calendar;

@ExtendWith(MockitoExtension.class)
public class ListAdsShould {

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void insert_and_delete_ad() {

    ListAds listAds = new ListAds();

    Id id = new Id("newAd");
    Date date = new Date(TimestampConverter.convert("24-04-2020"));
    Ad ad = new Ad.AdBuilder()
        .id(id)
        .title(new Title("title"))
        .description(new Description("description"))
        .date(date)
        .build();
    DTO.ListAds listAdsDTO = new DTO.ListAds();
    listAdsDTO.list.add(ad.createDTO());

    listAds.insert(ad);

    Assertions.assertEquals(ListAds.createFromDTO(listAdsDTO), listAds);
  }

  @Test
  public void select_ad_by_content() {

    ListAds listAds = new ListAds();
    Id id = new Id("newAd");
    Date date = new Date(TimestampConverter.convert("24-04-2020"));
    Ad ad = new Ad.AdBuilder()
        .id(id)
        .title(new Title("title"))
        .description(new Description("description"))
        .date(date)
        .build();
    DTO.ListAds listAdsDTO = new DTO.ListAds();
    listAdsDTO.list.add(ad.createDTO());

    listAds.insert(ad);

    Assertions.assertEquals(ad, listAds.selectBy("title","description"));
  }

  @Test
  public void not_found_ad_by_content() {

    ListAds listAds = new ListAds();

    Assertions.assertNull(listAds.selectBy("title","description"));
  }

  @Test
  public void delete_ad_correctly() {

    ListAds listAds = new ListAds();
    Id id = new Id("newAd");
    Date date = new Date(TimestampConverter.convert("24-04-2020"));
    Ad ad = new Ad.AdBuilder()
        .id(id)
        .title(new Title("title"))
        .description(new Description("description"))
        .date(date)
        .build();
    DTO.ListAds listAdsDTO = new DTO.ListAds();
    listAdsDTO.list.add(ad.createDTO());
    listAds.insert(ad);

    Assertions.assertEquals(1, listAds.delete(id.get()));
  }

  @Test
  public void not_found_ad_to_delete() {

    ListAds listAds = new ListAds();

    Assertions.assertEquals(0, listAds.delete("idNotFound"));
  }

  @Test
  public void expire_ads_by_date_correctly() {

    GeneratorID generatorID = new GeneratorUUID();
    ListAds listAds = new ListAds();
    Timestamp time = TimestampConverter.convert("01-01-2020");
    Calendar calendar = Calendar.getInstance();
    for (int i = 1; i < 60; i++) {
      calendar.setTime(time);
      calendar.add(Calendar.DAY_OF_WEEK, 1);
      time = new Timestamp(calendar.getTime().getTime());
      Ad ad = new Ad.AdBuilder()
          .id(new Id(i + generatorID.generate()))
          .title(new Title("title" + i))
          .description(new Description("description" + i))
          .date(new Date(time))
          .build();
      listAds.insert(ad);
    }
    ListAds listAdsExpected = new ListAds();
    Timestamp timeToExist = TimestampConverter.convert("29-02-2020");
    for (int i = 0; i < 3; i++) {
      calendar.setTime(timeToExist);
      calendar.add(Calendar.DAY_OF_WEEK, 1);
      timeToExist = new Timestamp(calendar.getTime().getTime());
      Ad ad = new Ad.AdBuilder()
          .id(new Id(i + generatorID.generate()))
          .title(new Title("title" + i))
          .description(new Description("description" + i))
          .date(new Date(timeToExist))
          .build();
      listAdsExpected.insert(ad);
    }
    Date expirationDate = new Date(TimestampConverter.convert("29-02-2020"));

    listAdsExpected.deleteOldestThan(expirationDate);

    Assertions.assertEquals(listAdsExpected, listAdsExpected);
  }
}
