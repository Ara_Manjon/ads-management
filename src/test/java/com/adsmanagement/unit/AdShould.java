package com.adsmanagement.unit;

import com.adsmanagement.adsmanager.domain.entities.ad.domain.entities.Ad;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.TitleAndDescriptionCanNotBeTheSame;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.exceptions.TitleCannotBeBiggerThan50;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Date;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Description;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Identifier.Id;
import com.adsmanagement.adsmanager.domain.entities.ad.domain.valueobjects.Title;
import com.adsmanagement.adsmanager.domain.expirator.timeconverter.TimestampConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AdShould {

  @Test
  public void throw_an_error_if_title_and_description_is_the_same() {

    Assertions.assertThrows(TitleAndDescriptionCanNotBeTheSame.class,
        () -> new Ad.AdBuilder()
            .id(new Id("123"))
            .title(new Title("title"))
            .description(new Description("title"))
            .date(new Date(TimestampConverter.convert("24-04-2020")))
            .build()
    );
  }

  @Test
  public void throw_an_error_if_title_has_not_valid_data() {

    Assertions.assertThrows(TitleCannotBeBiggerThan50.class,
        () -> new Ad.AdBuilder()
            .id(new Id("123"))
            .title(new Title("title**********************************************"))
            .description(new Description("description"))
            .date(new Date(TimestampConverter.convert("24-04-2020")))
            .build()
    );
  }
}
