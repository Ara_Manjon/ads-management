CREATE TABLE IF NOT EXISTS ads (
   id VARCHAR(50) NOT NULL UNIQUE,
   title VARCHAR(50) NOT NULL,
   description VARCHAR(50) NOT NULL,
   ad_date TIMESTAMP NOT NULL
);
