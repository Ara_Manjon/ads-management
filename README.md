# Ads Management
Ads Management is an API for manage the persistence of ads in DB. Written in Java and running in SQL and inMemoryRepository.
Currently, is in progress a client side app working it.

### Installation
> git clone git@gitlab.com:Ara_Manjon/ads-management.git
>
> cd ads-management/src/main
>
> run ApiApplication 
>
> install Postman to interact with REST API
>
> visit http://localhost:8080 and start to use endpoints
---------------------------------

### Description
The project has been separated in four parts with new requirements each one, simulating four sprints. You can watch process by the repository's branches flow.

#### :rocket: Sprint-1

Add, remove and list ads from a catalog. 

Note: An ad has a title, a description and a publication date.

##### Constraints:

:x: Cannot be two ads with same title and description in the catalog.

:x: The title should not be longer than 50 characters.

:x: Title and description cannot be the same.

##### Implementation:

:mag: From an outside-in TDD approach and starting by an acceptance test with all requirements.

:mag: The main one has been on creating a scalable code open to future requirements.

**Add an Ad**

> POST /ads

**Parameters**

| name | required | type |
|:-------|:-------|:-------|
|`title` |true |string | 
|`description` |true |string |

**List Ads**

> GET /ads

**Remove Ad**

> DELETE /ads/{adId}

#### :rocket: Sprint-2

As a result of how fast the catalog grows, news requirement arises to purge old ads.

Expiration strategy that given a date, removes all listAds published before this date.

The catalog has a maximum of 100 ads. 

##### Constraints:

:x: When maximum size has been reached and is tried to add an ad, then the oldest ad that currently exists has to be removed.

##### Implementation:

:mag: Create two acceptance test: 

- First -> update list ads published before a given date: the strategy was create a service to make expiration by date introduced.
- Second -> remove the oldest ad when maximum size is overcomes: is introduced a capacity value and is automatically requested when is added a new ad.

**Update list Ads**

> PATCH /ads

**Parameters**

| name | required | type |
|:-------|:-------|:-------|
|`date` |true |string | 